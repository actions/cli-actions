package main

import (
	"github.com/codegangsta/cli"
	// "github.com/crackcomm/go-actions/response"
	"log"
)

var runDesc = `Tries to get action from directories in ACTIONS_PATH and ACTIONS_CACHE (unless --no-cache flag).
   If action is not present locally it's requested from ACTIONS_STORE.
   If action was retrieved from remote source it's saved in ACTIONS_CACHE directory.
   Then command line arguments are parsed and passed as action arguments and it's executed.

EXAMPLE:
   $ actions run some.test
   $ actions run https://myapi.svc.com/v1/api/actions/some/test.json
   $ actions run ./some/test.json
   $ actions run --cloud=https://m01.mycloud.com:5001/run/ memory.stats
   => executes memory.stats on remote machine and prints result`

var runCommand = cli.Command{
	Name:      "run",
	ShortName: "r",
	Usage:     "runs an action",
	Flags: []cli.Flag{
		cli.StringFlag{"print, p", "pretty", "result print format"},
		cli.StringFlag{"format, f", "json", "action format (if file or url)"},
		cli.StringFlag{"cloud, c", "", "cloud endpoint (remote execution)"},
		// cli.BoolFlag{"remote, r", "execute remote (enabled if -c is set)"},
	},
	Description: runDesc,
	Action:      runHandler,
}

func runHandler(c *cli.Context) {
	name := c.Args().First()
	if name == "" {
		log.Fatalf("Action name can not be empty.")
	}

	// action, err := Registry.Get(name)
	// if err != nil {
	// 	log.Fatalf("Error finding action: %v", err)
	// }
	log.Printf("Action %s was successfuly found.", name)

	// action.Args = // update from cli args

	// var resp response.Response

	// if cloud
	// if inCloud {
	// 	resp = <-Cloud.Run(action)
	// } else {
	// 	resp = <-Local.Run(action)
	// }

	// if resp.Error != nil {
	// 	log.Fatalf("Error executing action: %v", resp.Error)
	// }

	log.Printf("Action %s was successfuly executed.", name)
}
