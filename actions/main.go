// Command line program for actions execution.
//
// Actions can be imported from sources defined in environment.
//
// Used environment variables:
//
// 	ACTIONS_STORE - list of actions stores (comma separated)
// 	ACTIONS_CACHE - directory containing actions from ACTIONS_STORE
// 	ACTIONS_CLOUD - url to actions run endpoint
// 	ACTIONS_ROOT  - directory containing actions added eq by "actions add" command
// 	ACTIONS_PATH  - list of directories containing actions (colon separated)
//
package main

import (
	"github.com/codegangsta/cli"
	"os"
)

var usage = `Command line program for running actions. Actions are imported from sources defined in environment.`

func main() {
	app := cli.NewApp()
	app.Usage = usage
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		addCommand,
		getCommand,
		runCommand,
		syncCommand,
		cloudCommand,
	}
	app.Run(os.Args)
}
