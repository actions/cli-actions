package main

import (
	"github.com/codegangsta/cli"
	"log"
)

var getDesc = `Gets action by it's name from local or remote sources.
   First tries to get action from directories in ACTIONS_PATH and ACTIONS_CACHE (unless --no-cache flag).
   If action is not present locally it's requested from ACTIONS_STORE (unless --local flag).
   If action was retrieved from remote source it's saved in ACTIONS_CACHE directory.

EXAMPLE:
   $ actions get some.test`

var getCommand = cli.Command{
	Name:      "get",
	ShortName: "a",
	Usage:     "get action from any source",
	Flags: []cli.Flag{
		cli.StringFlag{"format, f", "json", "action format"},
		cli.BoolFlag{"local, l", "get only from local sources"},
	},
	Description: getDesc,
	Action:      getHandler,
}

func getHandler(c *cli.Context) {
	name := c.Args().First()
	if name == "" {
		log.Fatalf("Action name can not be empty.")
	}

	// action, err := Registry.Get(name)
	// if err != nil {
	// 	log.Fatalf("Error finding action: %v", err)
	// }

	log.Printf("Action %s was successfuly found.", name)
	// log.Println(action)
}
