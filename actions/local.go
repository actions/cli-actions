package main

import (
	"github.com/crackcomm/go-actions/core"
	"github.com/crackcomm/go-actions/local"
)

// Local runner
//
// Runs actions.
var Local = &local.Runner{
	Core:     core.Default,
	Registry: Registry,
}
