package main

import (
	"github.com/crackcomm/go-actions/registry"
	"github.com/crackcomm/go-actions/source/file"
	"github.com/crackcomm/go-actions/source/http"
	"os"
	"strings"
)

var Registry = registry.NewRegistry()

// creates a registry using environment variables
//   ACTIONS_STORE - list of actions stores (comma separated)
//   ACTIONS_CACHE - directory containing actions from ACTIONS_STORE
//   ACTIONS_PATH  - list of directories containing actions (colon separated)
func init() {
	// Cache for actions from remote sources
	cache := os.Getenv("ACTIONS_CACHE")

	// Add http sources from ACTIONS_STORE environment variable
	urls := strings.Split(os.Getenv("ACTIONS_STORE"), ",")
	for _, url := range urls {
		// Pass if empty string
		if url == "" {
			continue
		}
		// Add file source
		Registry.Sources.Add(&http.Source{url, cache})
	}

	// Add file sources from ACTIONS_PATH environment variable
	paths := strings.Split(os.Getenv("ACTIONS_PATH"), ":")
	for _, path := range paths {
		// Pass if empty string
		if path == "" {
			continue
		}
		// Add file source
		Registry.Sources.Add(&file.Source{path})
	}
}
