package main

import (
	"github.com/codegangsta/cli"
	"log"
)

var addDesc = `Adds action from file or URL to local actions collection (ACTIONS_ROOT).
   First argument can be action name, action URL or path to action file.
   
EXAMPLE:
   $ actions add --name="some.test" https://myapi.svc.com/v1/api/actions/some/test.json
   $ actions add --name="some.test" ./some/test.json
   $ actions add --name="filmweb" filmweb_actions.json`

var addCommand = cli.Command{
	Name:      "add",
	ShortName: "a",
	Usage:     "add action to all sources",
	Flags: []cli.Flag{
		cli.StringFlag{"format, f", "json", "action format"},
	},
	Description: addDesc,
	Action:      addHandler,
}

func addHandler(c *cli.Context) {
	name := c.Args().First()
	if name == "" {
		log.Fatalf("Action name can not be empty.")
	}

	// err := Registry.Add(name, action)
	// if err != nil {
	// 	log.Fatalf("Error saving action: %v", err)
	// }

	log.Printf("Action %s was successfuly saved.", name)
}
