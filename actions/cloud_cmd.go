package main

import (
	"github.com/codegangsta/cli"
)

var cloudCommand = cli.Command{
	Name:        "cloud",
	ShortName:   "c",
	Usage:       "manage actions cloud",
	Description: "",
	Subcommands: []cli.Command{
		{
			Name:   "machines",
			Usage:  "manage cloud machines",
			Action: func(c *cli.Context) {},
		},
		{
			Name:   "add",
			Usage:  "add cloud machine",
			Action: func(c *cli.Context) {},
		},
	},
}
