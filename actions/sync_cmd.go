package main

import (
	"github.com/codegangsta/cli"
)

var syncCommand = cli.Command{
	Name:      "sync",
	ShortName: "s",
	Usage:     "syncs actions between local and remote sources",
	Subcommands: []cli.Command{
		{
			Name:  "remote",
			Usage: "sync actions to remote sources",
			Action: func(c *cli.Context) {
				println("new task template: ", c.Args().First())
			},
		},
		{
			Name:  "local",
			Usage: "sync actions from remote to local sources",
			Action: func(c *cli.Context) {
				println("removed task template: ", c.Args().First())
			},
		},
	},
}
