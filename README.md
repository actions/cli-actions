
# actions command line application

Application is written using [codegangsta/cli](https://github.com/codegangsta/cli/).

Main problems this app should solve is

1. Running actions on local machine
2. Running actions in the cloud
3. Storing actions in the cloud
4. Pulling actions from the cloud

## Install

	$ go get github.com/crackcomm/cli-actions/cmd/actions

## Usage

	$ actions run filmweb.find --title="Pulp Fiction"
